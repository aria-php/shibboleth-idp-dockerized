FROM jetty:9.3-jre8-alpine as temp

USER root

ENV idp_version=3.4.8 \
    idp_hash=ad0fcd834d0c6571363d47ad6dde08fbb75cce3202c41f8c64a5b42614f95a27 \
    dta_hash=2f547074b06952b94c35631398f36746820a7697 \
    slf4j_version=1.7.25 \
    slf4j_hash=da76ca59f6a57ee3102f8f9bd9cee742973efa8a \
    logback_version=1.2.3 \
    logback_classic_hash=7c4f3c474fb2c041d8028740440937705ebb473a \
    logback_core_hash=864344400c3d4d92dfeb0a305dc87d953677c03c \
    logback_access_hash=e8a841cb796f6423c7afd8738df6e0e4052bf24a \
    mysql_version=8.0.23 \
    mysql_hash=0856fa2e627c7ee78019cd0980d04614

ENV JETTY_MAX_HEAP=2048m \
    JETTY_BROWSER_SSL_KEYSTORE_PASSWORD=changeme \
    JETTY_BACKCHANNEL_SSL_KEYSTORE_PASSWORD=changeme

RUN apk update && apk add bash

# Download Shibboleth IdP, verify the hash, and install
RUN wget -q https://shibboleth.net/downloads/identity-provider/$idp_version/shibboleth-identity-provider-$idp_version.tar.gz \
    && echo "$idp_hash  shibboleth-identity-provider-$idp_version.tar.gz" | sha256sum -c - \
    && tar -zxvf  shibboleth-identity-provider-$idp_version.tar.gz -C /opt

# Download the library to allow SOAP Endpoints, verify the hash, and place
RUN wget -q https://build.shibboleth.net/nexus/content/repositories/releases/net/shibboleth/utilities/jetty9/jetty9-dta-ssl/1.0.0/jetty9-dta-ssl-1.0.0.jar \
    && echo "$dta_hash  jetty9-dta-ssl-1.0.0.jar" | sha1sum -c - \
    && mv ./jetty9-dta-ssl-1.0.0.jar ${JETTY_BASE}/lib/ext/

### LOGGING CONFIG

RUN mkdir -p ${JETTY_BASE}/lib/logging/

# Download the slf4j library for Jetty logging, verify the hash, and place
RUN wget -q https://repo1.maven.org/maven2/org/slf4j/slf4j-api/$slf4j_version/slf4j-api-$slf4j_version.jar \
    && echo "$slf4j_hash  slf4j-api-$slf4j_version.jar" | sha1sum -c - \
    && mv slf4j-api-$slf4j_version.jar ${JETTY_BASE}/lib/logging/

# Download the logback_classic library for Jetty logging, verify the hash, and place
RUN wget -q https://repo1.maven.org/maven2/ch/qos/logback/logback-classic/$logback_version/logback-classic-$logback_version.jar \
    && echo "$logback_classic_hash  logback-classic-$logback_version.jar" | sha1sum -c - \
    && mv logback-classic-$logback_version.jar ${JETTY_BASE}/lib/logging/

# Download the logback-core library for Jetty logging, verify the hash, and place
RUN wget -q https://repo1.maven.org/maven2/ch/qos/logback/logback-core/$logback_version/logback-core-$logback_version.jar \
    && echo "$logback_core_hash  logback-core-$logback_version.jar" | sha1sum -c - \
    && mv logback-core-$logback_version.jar ${JETTY_BASE}/lib/logging/

# Download the logback-access library for Jetty logging, verify the hash, and place
RUN wget -q https://repo1.maven.org/maven2/ch/qos/logback/logback-access/$logback_version/logback-access-$logback_version.jar \
    && echo "$logback_access_hash  logback-access-$logback_version.jar" | sha1sum -c - \
    && mv logback-access-$logback_version.jar ${JETTY_BASE}/lib/logging/

### END LOGGING CONIFG

COPY opt/shib-jetty-base/ ${JETTY_BASE}
COPY opt/shibboleth-idp/ /opt/shibboleth-idp/

# Download and install mySQL
RUN wget -q https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-${mysql_version}.tar.gz \
# TODO: Check the hash
#    && echo ${mysql_hash} | md5sum
    && mkdir -p /opt/shibboleth-idp/edit-webapp/WEB-INF/lib/ \
    && tar -zxvf mysql-connector-java-${mysql_version}.tar.gz mysql-connector-java-${mysql_version}/mysql-connector-java-${mysql_version}.jar --strip-components=1 -C /opt/shibboleth-idp/edit-webapp/WEB-INF/lib/

# Build the war file
RUN /opt/shibboleth-identity-provider-$idp_version/bin/install.sh \
    -Didp.src.dir=/opt/shibboleth-identity-provider-$idp_version/ \
    -Didp.entityID=localhost \
    -Didp.host.name=temp \
    -Didp.keystore.password=${JETTY_BACKCHANNEL_SSL_KEYSTORE_PASSWORD} \
    -Didp.sealer.password=changeme \
    -Didp.noprompt=true \
    -Didp.target.dir=/opt/shibboleth-idp

# Forcibly re-copy the css into the container
COPY opt/shibboleth-idp/ /opt/shibboleth-idp/

# Setting owner ownership and permissions on new items in this command
RUN chown -R jetty:jetty /opt

# Setting owner ownership and permissions on new items from the COPY command
RUN mkdir $JETTY_BASE/logs \
    && chown -R jetty:jetty $JETTY_BASE

USER jetty

# Config Jetty
RUN mkdir -p ${JETTY_BASE}/modules ${JETTY_BASE}/lib/ext  ${JETTY_BASE}/lib/logging ${JETTY_BASE}/resources \
    && cd ${JETTY_BASE} \
    && java -jar ${JETTY_HOME}/start.jar --add-to-startd=http,https,deploy,ext,annotations,jstl,rewrite

LABEL maintainer="Instruct-ERIC"\
    #   idp.java.version="8.0.212" \
    #   idp.jetty.version="9.3.27.v20190418" \
      idp.version="3.4.8"

ENV JETTY_ARGS="jetty.sslContext.keyStorePassword=$JETTY_BROWSER_SSL_KEYSTORE_PASSWORD jetty.backchannel.sslContext.keyStorePassword=$JETTY_BACKCHANNEL_SSL_KEYSTORE_PASSWORD"

EXPOSE 8080 4443
