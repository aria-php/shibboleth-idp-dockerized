<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><spring:message code="${root.title}" text="Instruct Authentication Service" /></title>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/main.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/instruct/css/main.css">
  </head>

  <body>
    <header>
      <div class="content">
        <h1>Instruct</h1>
      </div>
    </header>
  
    <div class="content">
      <div class="box">
        <h2>Instruct Authentication Service</h2>
        <p class="error"><spring:message code="${root.message}" text="No services are available at this location." /></p>
      </div>
    </div>

    <footer>
      <div class="content">
        <p><spring:message code="${root.footer}" text="Copyright &copy; 2021 Instruct-ERIC.<br>Please contact us with any comments or questions you have about the site." /></p>
      </div>
    </footer>

  </body>
</html>
